# Generated by Django 2.0.6 on 2019-04-04 12:33

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0022_auto_20190320_1603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='date',
            field=models.DateTimeField(verbose_name=datetime.datetime(2019, 4, 4, 12, 33, 0, 655501, tzinfo=utc)),
        ),
    ]
